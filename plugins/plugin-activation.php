<?php
require_once get_template_directory() . '/plugins/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'lawyerwp_register_required_plugins' );
function lawyerwp_register_required_plugins() {
$plugins = array(
array(
'name'               => 'Divi Builder',
'slug'               => 'divi-builder',
'source'             => 'https://www.elegantthemes.com/api/api_downloads.php?api_update=1&theme=divi-builder&api_key=03e4dbc0d4d8117b2275bda926d803c3bb404a66&username=divikey',
'required'           => true,
),
array(
'name'               => 'Maintenance',
'slug'               => 'maintenance',
'required'           => false,
),
array(
'name'               => 'Lexicata',
'slug'               => 'lexicata',
'required'           => false,
),
array(
'name'               => 'WooCommerce',
'slug'               => 'woocommerce',
'required'           => false,
),
array(
'name'               => 'bbPress',
'slug'               => 'bbpress',
'required'           => false,
),
array(
'name'               => 'Meteor Slides',
'slug'               => 'meteor-slides',
'required'           => false,
),
array(
'name'               => 'All in One SEO Pack',
'slug'               => 'all-in-one-seo-pack',
'required'           => false,
),
array(
'name'               => 'Google XML Sitemaps',
'slug'               => 'google-sitemap-generator',
'required'           => false,
),
array(
'name'               => 'W3 Total Cache',
'slug'               => 'w3-total-cache',
'required'           => false,
),
array(
'name'               => 'Stop Spammers',
'slug'               => 'stop-spammer-registrations-plugin',
'required'           => false,
),
array(
'name'               => 'Contact Form 7',
'slug'               => 'contact-form-7',
'required'           => false,
),
array(
'name'               => 'Theme My Login',
'slug'               => 'theme-my-login',
'required'           => false,
),
array(
'name'               => 'Share Buttons by AddThis',
'slug'               => 'addthis',
'required'           => false,
),
array(
'name'               => 'Easy SwipeBox',
'slug'               => 'easy-swipebox',
'required'           => false,
)
);
$config = array(
'domain'                          => 'lawyerpro',
'menu'                            => 'install-plugins',
'has_notices'                     => true,
'is_automatic'                    => true,
'strings'                         => array(
'page_title'                      => esc_html__( 'Install Required Plugins', 'lawyerpro' ),
'menu_title'                      => esc_html__( 'Install Plugins', 'lawyerpro' ),
'installing'                      => esc_html__( 'Installing Plugin: %s', 'lawyerpro' ),
'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'lawyerpro' ),
'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'lawyerpro' ),
'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'lawyerpro' ),
'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'lawyerpro' ),
'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'lawyerpro' ),
'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'lawyerpro' ),
'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'lawyerpro' ),
'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'lawyerpro' ),
'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'lawyerpro' ),
'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'lawyerpro' ),
'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'lawyerpro' ),
'return'                          => esc_html__( 'Return to Required Plugins Installer', 'lawyerpro' ),
'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'lawyerpro' ),
'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'lawyerpro' ),
'nag_type'                        => 'updated'
)
);
tgmpa( $plugins, $config );
}