<?php /* Template Name Posts: Login */ ?>
<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<script type="text/javascript">
jQuery(document).ready(function($) {
$(".tab_content_login").hide();
$("ul.tabs_login li:first").addClass("active_login").show();
$(".tab_content_login:first").show();
$("ul.tabs_login li").click(function() {
$("ul.tabs_login li").removeClass("active_login");
$(this).addClass("active_login");
$(".tab_content_login").hide();
var activeTab = $(this).find("a").attr("href");
if ($.browser.msie) {$(activeTab).show();}
else {$(activeTab).show();}
return false;
});
});
</script>
<div id="login-register-password">
<?php global $user_ID, $user_identity; wp_get_current_user(); if ( !$user_ID ) { ?>
<ul class="tabs_login">
<li class="active_login"><a href="#login">Login</a></li>
<li><a href="#register">Register</a></li>
<li><a href="#forgot">Forgot Password?</a></li>
</ul>
<div class="tab_container_login">
<div id="login" class="tab_content_login">
<?php $register = $_GET['register']; $reset = $_GET['reset']; if ( $register == true ) { ?>
<h3>Success</h3>
<p>Check your email for the password and return to login.</p>
<?php } elseif ( $reset == true ) { ?>
<h3>Success</h3>
<p>Check your email to reset your password.</p>
<?php } else { ?>
<h3>Login</h3>
<?php } ?>
<form method="post" action="<?php echo esc_url( home_url() ); ?>/wp-login.php" class="wp-user-form">
<div class="username">
<label for="user_login"><?php esc_html_e( 'Username', 'lawyerpro' ); ?>: </label>
<input type="text" name="log" value="<?php echo esc_attr( stripslashes( $user_login ) ); ?>" size="20" id="user_login" tabindex="11" />
</div>
<div class="password">
<label for="user_pass"><?php esc_html_e( 'Password', 'lawyerpro' ); ?>: </label>
<input type="password" name="pwd" value="" size="20" id="user_pass" tabindex="12" />
</div>
<div class="login_fields">
<div class="rememberme">
<label for="rememberme">
<input type="checkbox" name="rememberme" value="forever" id="rememberme" tabindex="13" /> Remember Me
</label>
</div>
<?php do_action( 'login_form' ); ?>
<input type="submit" name="user-submit" value="<?php esc_html_e( 'Login', 'lawyerpro' ); ?>" tabindex="14" class="user-submit" />
<input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" />
<input type="hidden" name="user-cookie" value="1" />
</div>
</form>
</div>
<div id="register" class="tab_content_login" style="display:none">
<h3>Register</h3>
<form method="post" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" class="wp-user-form">
<div class="username">
<label for="user_login"><?php esc_html_e( 'Username', 'lawyerpro' ); ?>: </label>
<input type="text" name="user_login" value="<?php echo esc_attr( stripslashes( $user_login ) ); ?>" size="20" id="user_login" tabindex="101" />
</div>
<div class="password">
<label for="user_email"><?php esc_html_e( 'Email', 'lawyerpro' ); ?>: </label>
<input type="text" name="user_email" value="<?php echo esc_attr( stripslashes( $user_email ) ); ?>" size="25" id="user_email" tabindex="102" />
</div>
<div class="login_fields">
<?php do_action( 'register_form' ); ?>
<input type="submit" name="user-submit" value="<?php esc_html_e( 'Register', 'lawyerpro' ); ?>" class="user-submit" tabindex="103" />
<?php $register = $_GET['register']; if ( $register == true ) { echo '<p>Check your email for the password.</p>'; } ?>
<input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>?register=true" />
<input type="hidden" name="user-cookie" value="1" />
</div>
</form>
</div>
<div id="forgot" class="tab_content_login" style="display:none;">
<h3>Forgot Password?</h3>
<form method="post" action="<?php echo esc_url( site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" class="wp-user-form">
<div class="forgot">
<label for="user_login" class="hide"><?php esc_html_e( 'Username or Email', 'lawyerpro' ); ?>: </label>
<input type="text" name="user_login" value="" size="20" id="user_login" tabindex="1001" />
</div>
<div class="login_fields">
<?php do_action( 'login_form', 'resetpass' ); ?>
<input type="submit" name="user-submit" value="<?php esc_html_e( 'Reset Password', 'lawyerpro' ); ?>" class="user-submit" tabindex="1002" />
<?php $reset = $_GET['reset']; if ( $reset == true ) { echo '<p>A message will be sent to your email address.</p>'; } ?>
<input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>?reset=true" />
<input type="hidden" name="user-cookie" value="1" />
</div>
</form>
</div>
</div>
<?php } else { // is logged in ?>
<div class="sidebox">
<h3>Welcome, <?php echo esc_html( $user_identity ); ?></h3>
<div class="usericon">
<?php global $userdata; wp_get_current_user(); echo get_avatar( $userdata->ID, 60 ); ?>
</div>
<div class="userinfo">
<p>You&rsquo;re logged in as <strong><?php echo esc_html( $user_identity ); ?></strong></p>
<p>
<a href="<?php echo esc_url( wp_logout_url( get_permalink() ) ); ?>">Logout</a>
<?php if ( current_user_can( 'manage_options' ) ) { 
echo '<a href="' . esc_url( admin_url() ) . '">' . esc_html__( 'Admin', 'lawyerpro' ) . '</a>'; } else { 
echo '<a href="' . esc_url( admin_url() ) . 'profile.php">' . esc_html__( 'Profile', 'lawyerpro' ) . '</a>'; } ?>
</p>
</div>
</div>
<?php } ?>
</div>
<?php get_template_part( 'entry' ); ?>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
<footer class="footer">
<?php get_template_part( 'nav', 'below-single' ); ?>
</footer>
</section>
<?php get_footer(); ?>