<?php
add_action( 'admin_init', 'lawyerwp_admin_init' );
function lawyerwp_admin_init()
{
add_meta_box( 'lawyerwp_post_template', esc_html__( 'Post Template', 'lawyerpro' ), 'lawyerwp_post_template', 'post', 'side', 'default' );
}
function lawyerwp_post_template( $post )
{
$post_id = $post->ID;
$template_vars = array();
$templates = lawyerwp_get_post_templates();
$custom_template = lawyerwp_get_custom_post_template( $post->ID );
?>
<label class="hidden" for="page_template"><?php esc_html_e( 'Post Template', 'lawyerpro' ); ?></label>
<?php if ( $templates ) : ?>
<input type="hidden" name="custom_post_template_present" value="1" />
<select name="custom_post_template" id="custom_post_template">
<option value='default'	<?php if( !$custom_template ) { echo "selected='selected'"; } ?>><?php esc_html_e( 'Default Template', 'lawyerpro' ); ?></option>
<?php foreach( $templates as $filename => $name ) { ?>
<option value='<?php echo esc_html( $filename ); ?>' <?php if ( $custom_template == $filename ) { echo "selected='selected'"; }?>><?php echo esc_html( $name ); ?></option>
<?php } ?>
</select>
<?php else : ?>
<p><?php esc_html_e( 'This theme has no available custom post templates.', 'lawyerpro' ); ?></p>
<?php endif; ?>
<?php
}
function lawyerwp_is_post_template( $template = '' )
{
if ( !is_single() ) {
return false;
}
global $wp_query;
$post = $wp_query->get_queried_object();
$post_template = get_post_meta( $post->ID, 'lawyerwp_custom_post_template', true );
if ( empty( $template ) ) {
if ( !empty( $post_template ) ) {
return true;
}
} elseif ( $template == $post_template ) {
return true;
}
return false;
}
add_filter( 'body_class', 'lawyerwp_body_class' );
function lawyerwp_body_class( $classes )
{
if ( ! lawyerwp_is_post_template() )
return $classes;
global $wp_query;
$post = $wp_query->get_queried_object();
$post_template = get_post_meta( $post->ID, 'lawyerwp_custom_post_template', true );
$classes[] = 'post-template';
$classes[] = 'post-template-' . str_replace( ['templates', '/', '-post', '.php'], '', $post_template );
$classes[] = 'post-template-' . str_replace( '.php', '-php', str_replace( '/', '', $post_template ) );
return $classes;
}
add_action( 'save_post', 'lawyerwp_save_post' );
function lawyerwp_save_post( $post_ID )
{
$action_needed = (bool) @ $_POST[ 'custom_post_template_present' ];
if ( ! $action_needed ) return;
$template = (string) @ $_POST[ 'custom_post_template' ];
lawyerwp_set_custom_post_template( $template, $post_ID );
}
add_filter( 'single_template', 'lawyerwp_filter_single_template' );
function lawyerwp_filter_single_template( $template )
{
global $wp_query;
$template_file = lawyerwp_get_custom_post_template( $wp_query->post->ID );
if ( ! $template_file )
return $template;
if ( file_exists( trailingslashit( STYLESHEETPATH ) . $template_file ) )
return STYLESHEETPATH . DIRECTORY_SEPARATOR . $template_file;
else if ( file_exists( TEMPLATEPATH . DIRECTORY_SEPARATOR . $template_file ) )
return TEMPLATEPATH . DIRECTORY_SEPARATOR . $template_file;
return $template;
}
function lawyerwp_set_custom_post_template( $template, $post_ID )
{
delete_post_meta( $post_ID, 'lawyerwp_custom_post_template' );
if ( ! $template || $template == 'default' ) return;
add_post_meta( $post_ID, 'lawyerwp_custom_post_template', $template );
}
function lawyerwp_get_custom_post_template( $post_id )
{
$custom_template = get_post_meta( $post_id, 'lawyerwp_custom_post_template', true );
return $custom_template;
}
function lawyerwp_get_post_templates()
{
$theme = wp_get_theme();
$post_templates = array();
$files = (array) $theme->get_files( 'php', 1 );
foreach ( $files as $file => $full_path ) {
if ( ! preg_match( '|Template Name Posts:(.*)$|mi', file_get_contents( $full_path ), $header ) ) continue;
if ($file == 'templates/post-templates.php') continue;
$post_templates[ $file ] = _cleanup_header_comment( $header[1] );
}
return $post_templates;
}
?>