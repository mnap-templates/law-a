<?php /* Template Name: Creative */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php the_post(); ?>
<?php the_content(); ?>
<?php edit_post_link(); ?>
<?php wp_footer(); ?>
</body>
</html>