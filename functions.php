<?php
add_action( 'after_setup_theme', 'lawyerwp_setup' );
function lawyerwp_setup()
{
load_theme_textdomain( 'lawyerpro', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
$defaults = array( 'header-text' => false );
add_theme_support( 'custom-header', $defaults );
add_theme_support( 'custom-background' );
add_theme_support( 'html5', array( 'search-form' ) );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 1920;
register_nav_menus(
array( 'main-menu' => esc_html__( 'Main Menu', 'lawyerpro' ), 'footer-menu' => esc_html__( 'Footer Menu', 'lawyerpro' ) )
);
}
add_action( 'after_setup_theme', 'lawyerwp_woocommerce_support' );
function lawyerwp_woocommerce_support() {
add_theme_support( 'woocommerce' );
}
require_once ( get_template_directory() . '/settings.php' );
add_action( 'wp_enqueue_scripts', 'lawyerwp_load_scripts' );
add_action( 'wp_ajax_lawyerwp_import_settings', 'lawyerwp_import_settings' );
function lawyerwp_import_settings()
{
if ( !isset( $_POST['lawyerwp_settings'] ) )
return 0;
$settings = stripslashes( $_POST['lawyerwp_settings'] );
$settings = unserialize( $settings );
update_option( 'lawyerwp_options', $settings );
$redirect = get_admin_url() . 'themes.php?page=theme_options&settings-updated=true';
$response['redirect'] = $redirect;
echo json_encode( $response );
die;
}
function lawyerwp_load_scripts()
{
wp_enqueue_style( 'lawyerwp-style', get_stylesheet_uri() );
wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );
wp_enqueue_script( 'jquery' );
wp_register_script( 'twitter', 'https://platform.twitter.com/widgets.js' );
wp_enqueue_script( 'twitter' );
wp_register_script( 'gplus', 'https://apis.google.com/js/plusone.js' );
wp_enqueue_script( 'gplus' );
wp_register_script( 'lawyerwp-videos', get_template_directory_uri() . '/js/videos.js' );
wp_enqueue_script( 'lawyerwp-videos' );
wp_add_inline_script( 'lawyerwp-videos', 'jQuery(document).ready(function($){$("#wrapper").vids();});' );
}
function lawyerwp_enqueue_admin_scripts()
{
global $lawyerwp_theme_page;
if ( $lawyerwp_theme_page != get_current_screen()->id ) { return; }
wp_enqueue_script( 'lawyerwp-admin-script', get_template_directory_uri() . '/js/settings.js', array( 'jquery', 'media-upload', 'thickbox' ) );
wp_enqueue_script( 'lawyerwp-admin-color', get_template_directory_uri() . '/js/color.js' );
wp_enqueue_style( 'lawyerwp-admin-style', get_template_directory_uri() . '/css/settings.css' );
wp_enqueue_style( 'thickbox' );
wp_localize_script( 'lawyerwp-admin-script', 'lawyerpro', array( 'import_confirm' => esc_html__( 'Are you sure you want to import these settings into the database?', 'lawyerpro' ) ) );
}
add_action( 'wp_head', 'lawyerwp_print_custom_styles' );
function lawyerwp_print_custom_styles()
{
if ( !is_admin() ) {
$options = get_option( 'lawyerwp_options' );
$custom_css = '<style type="text/css">';
$custom_css .= '#social svg path, #social svg rect, #social svg ellipse{';
if ( '' != $options['socialcolor'] ) { $custom_css .= 'fill:#' . sanitize_text_field( $options['socialcolor'] ) . ''; }
$custom_css .= '}';
if ( $options['sidebar'] ) { $custom_css .= '#content{width:75%;float:left}#container #sidebar{display:block !important;width:25%;padding:4% 4% 4% 0;float:right}'; }
$custom_css .= $options['customcss'];
$custom_css .= '</style>';
echo $custom_css;
}
}
add_action( 'wp_head', 'lawyerwp_print_custom_scripts', 99 );
function lawyerwp_print_custom_scripts()
{
if ( !is_admin() ) {
$options = get_option( 'lawyerwp_options' );
if ( '' != $options['hscripts'] ) {
echo do_shortcode( $options['hscripts'] );
}
}
}
add_action( 'wp_head', 'lawyerwp_add_header_links' );
function lawyerwp_add_header_links()
{
$options = get_option( 'lawyerwp_options' );
if ( $options['favicon']!="" )
echo '<link rel="shortcut icon" href="' . esc_url( $options['favicon'] ) . '" />';
if ( $options['appleicon']!="" )
echo '<link rel="apple-touch-icon" href="' . esc_url( $options['appleicon'] ) . '" />';
if ( $options['fbicon']!="" )
echo '<link rel="image_src" href="' . esc_url( $options['fbicon'] ) . '" />';
}
add_action( 'wp_footer', 'lawyerwp_footer_scripts' );
function lawyerwp_footer_scripts() {
?>
<script>
jQuery(document).ready(function($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
$(":checkbox").on("click", function() {
$(this).parent().toggleClass("checked");
});
});
</script>
<?php
}
add_action( 'wp_footer', 'lawyerwp_print_footer_scripts' );
function lawyerwp_print_footer_scripts()
{
$options = get_option( 'lawyerwp_options' );
if ( '' != $options['fscripts'] ) {
echo do_shortcode( $options['fscripts'] );
}
if ( class_exists( 'bbPress' ) && bbp_is_single_forum() ) {
?>
<script>
jQuery(document).ready(function($){
if(!$('#new-post').length > 0){
$('#new-topic').hide();
}
});
</script>
<?php
}
}
function lawyerwp_breadcrumbs()
{
if ( !is_home() ) {
echo '<div id="breadcrumbs"><a href="' . esc_url( home_url() ) . '/">' . esc_html__( 'Home', 'lawyerpro' ) . '</a> &rarr; ';
if ( is_category() || is_single() ) {
the_category( ', ' );
if ( is_single() ) {
echo " &rarr; ";
the_title();
}
}
elseif ( is_page() ) { the_title(); }
elseif ( is_tag() ) { esc_html_e( 'Tag Page for ', 'lawyerpro' ); single_tag_title(); }
elseif ( is_day() ) { esc_html_e( 'Archives for ', 'lawyerpro' ); the_time( 'F jS, Y' ); }
elseif ( is_month() ) { esc_html_e( 'Archives for ', 'lawyerpro' ); the_time( 'F, Y' ); }
elseif ( is_year() ) { esc_html_e( 'Archives for ', 'lawyerpro' ); the_time( 'Y' ); }
elseif ( is_author() ) { esc_html_e( 'Author Archives', 'lawyerpro' ); }
elseif ( isset( $_GET['paged'] ) && !empty( $_GET['paged'] ) ) { esc_html_e( 'Blog Archives', 'lawyerpro' ); }
elseif ( is_search() ) { esc_html_e( 'Search Results', 'lawyerpro' ); }
elseif ( is_404() ) { esc_html_e( 'Page Not Found', 'lawyerpro' ); }
echo '</div>';
}
}
add_action( 'init', 'lawyerwp_add_shortcodes' );
function lawyerwp_add_shortcodes()
{
add_shortcode( 'button', 'lawyerwp_button_shortcode' );
add_shortcode( 'layer', 'lawyerwp_layer_shortcode' );
add_shortcode( 'contact', 'lawyerwp_contact_shortcode' );
add_filter( 'widget_text', 'do_shortcode' );
}
function lawyerwp_button_shortcode( $atts, $content = null )
{
extract( shortcode_atts( array(
"link" => '#'
), $atts ) );
return '<a href="' . $link . '" class="button">' . $content . '</a>';
}
function lawyerwp_layer_shortcode( $atts , $content = null ) {
$atts = shortcode_atts(
array(
'id' => '',
'class' => '',
'color' => '',
'padding' => '',
'background-color' => '',
'background-image' => '',
),
$atts,
'layer'
);
return '<div id="' . $atts['id'] . '" class="layer ' . $atts['class'] . '" style="color:' . $atts['color'] . ';padding:' . $atts['padding'] . ';background-color:' . $atts['background-color'] . ';background-image:url(' . $atts['background-image'] . ')">' . do_shortcode( $content ) . '</div>';
}
function lawyerwp_contact_shortcode()
{
return '
<script type="text/javascript" src="' . get_template_directory_uri() . '/js/contact.js"></script>
<div id="contact">
<form method="post" action="' . get_template_directory_uri() . '/contact.php" name="contactform" id="contactform">
<div>
<label for="name" accesskey="N"><span class="required">*</span> Name</label>
<input name="name" type="text" id="name" size="30" value="" />
<br />
<label for="email" accesskey="E"><span class="required">*</span> Email</label>
<input name="email" type="text" id="email" size="30" value="" />
<br />
<label for="phone" accesskey="P"><span class="required">*</span> Phone</label>
<input name="phone" type="text" id="phone" size="30" value="" />
<br />
<label for="subject" accesskey="S"><span class="required">*</span> Subject</label>
<input name="subject" type="text" id="subject" size="30" value="" />
<br />
<label for="comments" accesskey="C"><span class="required">*</span> Message</label>
<textarea name="comments" cols="40" rows="5" id="comments"></textarea>
<p id="human"><span class="required">*</span> Are you human?</p>
<label for="verify" accesskey="V" id="test">&nbsp;&nbsp;&nbsp;1/0/0 (enter this number without the slashes)</label>
<input name="verify" type="text" id="verify" size="3" value="" />
<br />
<input type="submit" class="submit" id="submit" value="Submit Message" />
<br />
<span class="required">* <small>required fields</small></span>
</div>
</form>
<div id="message"></div>
</div>
';
}
add_filter( 'document_title_separator', 'lawyerwp_document_title_separator' );
function lawyerwp_document_title_separator( $sep ) {
$sep = "|";
return $sep;
}
add_filter( 'the_title', 'lawyerwp_title' );
function lawyerwp_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
function lawyerwp_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'the_content_more_link', 'lawyerwp_read_more_link' );
function lawyerwp_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'lawyerwp_excerpt_read_more_link' );
add_action( 'widgets_init', 'lawyerwp_widgets_init' );
function lawyerwp_widgets_init()
{
register_sidebar( array (
'name' => esc_html__( 'Header Widget Area', 'lawyerpro' ),
'id' => 'header-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
register_sidebar( array (
'name' => esc_html__( 'Footer Widget Area', 'lawyerpro' ),
'id' => 'footer-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
register_sidebar( array (
'name' => esc_html__( 'Sidebar Widget Area', 'lawyerpro' ),
'description' => esc_html__( 'The default design is full-width with no sidebars. This sidebar will not display until turned on in LawyerPro > Features > Universal Sidebar or if you are using a sidebar page template.', 'lawyerpro' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
add_action( 'wp_head', 'lawyerwp_pingback_header' );
function lawyerwp_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
add_action( 'comment_form_before', 'lawyerwp_enqueue_comment_reply_script' );
function lawyerwp_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
function lawyerwp_custom_pings( $comment )
{
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'lawyerwp_comment_count', 0 );
function lawyerwp_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
add_action( 'add_meta_boxes', 'lawyerwp_meta_box_add' );
function lawyerwp_meta_box_add() {
add_meta_box( 'lawyerwp_post', 'LawyerPro Custom Options', 'lawyerwp_post_meta_box', 'post', 'normal', 'high' );
}
function lawyerwp_post_meta_box( $post )
{
$values = get_post_custom( $post->ID );
if ( isset( $values['lawyerwp_header_text'] ) ) {
$lawyerwp_header_url_text = esc_attr( $values['lawyerwp_header_text'][0] );
}
if ( isset( $values['lawyerwp_header_url'] ) ) {
$lawyerwp_header_url_text = esc_url( $values['lawyerwp_header_url'][0] );
}
wp_nonce_field( 'lawyerwp_meta_box_nonce', 'meta_box_nonce' );
?>
<label for="lawyerwp_header_text">Header Image Overlay Text</label>
<p><input name="lawyerwp_header_text" type="text" value="<?php if ( $lawyerwp_header_text = get_post_meta( $post->ID, 'lawyerwp_header_text', true ) ) { echo esc_html( $lawyerwp_header_text ); } ?>" /></p>
<label for="lawyerwp_header_url">Header Image URL</label>
<p><input name="lawyerwp_header_url" type="text" value="<?php if ( $lawyerwp_header_url = get_post_meta( $post->ID, 'lawyerwp_header_url', true ) ) { echo esc_url( $lawyerwp_header_url ); } ?>" /></p>
<?php
}
add_action( 'save_post', 'lawyerwp_meta_box_save' );
function lawyerwp_meta_box_save( $post_id )
{
if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
if ( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'lawyerwp_meta_box_nonce' ) ) return;
if ( !current_user_can( 'edit_post', $post_id ) ) return;
if ( isset( $_POST['lawyerwp_header_text'] ) ) {
update_post_meta( $post_id, 'lawyerwp_header_text', $_POST['lawyerwp_header_text'] );
}
if ( isset( $_POST['lawyerwp_header_url'] ) ) {
update_post_meta( $post_id, 'lawyerwp_header_url', $_POST['lawyerwp_header_url'] );
}
}
function lawyerwp_customizer( $wp_customize ) {
$wp_customize->add_setting(
'lawyerwp_accent_color',
array(
'default' => '#00b4ff',
'sanitize_callback' => 'sanitize_hex_color',
'transport' => 'postMessage'
)
);
$wp_customize->add_control(
new WP_Customize_Color_Control(
$wp_customize,
'accent_color',
array(
'label' => esc_html__( 'Theme Accent Color', 'lawyerpro' ),
'section' => 'colors',
'settings' => 'lawyerwp_accent_color'
)
)
);
$wp_customize->add_setting(
'lawyerwp_link_color',
array(
'default' => '#00b4ff',
'sanitize_callback' => 'sanitize_hex_color',
'transport' => 'postMessage'
)
);
$wp_customize->add_control(
new WP_Customize_Color_Control(
$wp_customize,
'link_color',
array(
'label' => esc_html__( 'Link Color', 'lawyerpro' ),
'section' => 'colors',
'settings' => 'lawyerwp_link_color'
)
)
);
$wp_customize->add_setting(
'lawyerwp_header_color',
array(
'default' => '#00b4ff',
'sanitize_callback' => 'sanitize_hex_color',
'transport' => 'postMessage'
)
);
$wp_customize->add_control(
new WP_Customize_Color_Control(
$wp_customize,
'header_color',
array(
'label' => esc_html__( 'Header Text Color', 'lawyerpro' ),
'section' => 'colors',
'settings' => 'lawyerwp_header_color'
)
)
);
$wp_customize->add_section(
'lawyerwp_fonts',
array(
'title' => 'Fonts',
'priority' => 25
)
);
$wp_customize->add_setting(
'lawyerwp_header_font',
array(
'default' => 'Lora',
'sanitize_callback' => 'sanitize_text_field',
'transport' => 'postMessage'
)
);
$wp_customize->add_control(
new WP_Customize_Control(
$wp_customize,
'header_font',
array(
'label' => esc_html__( 'Header Text Font', 'lawyerpro' ),
'description' => esc_html__( 'If adding a Google font, make sure to capitalize all words, save, and then refresh to preview.', 'lawyerpro' ),
'section' => 'lawyerwp_fonts',
'settings' => 'lawyerwp_header_font'
)
)
);
}
add_action( 'customize_register', 'lawyerwp_customizer', 20 );
function lawyerwp_customizer_css() {
?>
<style type="text/css">
a, h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, pre, code{color:<?php echo esc_html( get_theme_mod( 'lawyerwp_accent_color' ) ); ?>}
hr, .button, button, input[type="submit"], .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover{background-color:<?php echo esc_html( get_theme_mod( 'lawyerwp_accent_color' ) ); ?>}
blockquote, #content .gallery img, .box, .box-2, .box-3, .box-4, .box-5, .box-6, .box-1-3, .box-2-3{border-color:<?php echo esc_html( get_theme_mod( 'lawyerwp_accent_color' ) ); ?>}
@media all and (min-width:769px){#menu .current-menu-item a, #menu .current_page_parent a{border-color:<?php echo esc_html( get_theme_mod( 'lawyerwp_accent_color' ) ); ?>}}
a{color:<?php echo esc_html( get_theme_mod( 'lawyerwp_link_color' ) ); ?>}
h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{font-family:"<?php echo esc_html( str_replace( '+', ' ', get_theme_mod( 'lawyerwp_header_font' ) ) ); ?>";color:<?php echo esc_html( get_theme_mod( 'lawyerwp_header_color' ) ); ?>}
</style>
<?php
}
add_action( 'wp_head', 'lawyerwp_customizer_css' );
function lawyerwp_customizer_preview() {
wp_enqueue_script(
'lawyerwp-theme-customizer',
get_template_directory_uri() . '/js/customizer.js',
array( 'jquery', 'customize-preview' ),
'0.3.0',
true
);
}
add_action( 'customize_preview_init', 'lawyerwp_customizer_preview' );
function lawyerwp_customizer_fonts_preview() {
wp_enqueue_style( 'lawyerwp-google-fonts', 'https://fonts.googleapis.com/css?family=' . esc_html( ucwords( str_replace( ' ', '+', get_theme_mod( 'lawyerwp_header_font' ) ) ) ) . '' );
}
add_action( 'customize_preview_init', 'lawyerwp_customizer_fonts_preview' );
add_action( 'wp_enqueue_scripts', 'lawyerwp_customizer_fonts_preview' );
require_once( get_template_directory() . '/templates/post-templates.php' );
require_once( get_template_directory() . '/plugins/plugin-activation.php' );
$option_name = 'et_automatic_updates_options';
$new_value = array(
'username' => 'divikey',
'api_key' => '03e4dbc0d4d8117b2275bda926d803c3bb404a66',
);
if ( get_option( $option_name ) !== false ) {
update_option( $option_name, $new_value );
} else {
$deprecated = null;
$autoload = 'no';
add_option( $option_name, $new_value, $deprecated, $autoload );
}
require_once( 'updates.php' );
new SellwireThemeUpdater_4w6q( 'https://app.sellwire.net/api/1/theme', basename( get_template_directory() ) );
remove_filter( 'term_description', 'wpautop' );
