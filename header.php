<?php $options = get_option('lawyerwp_options'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if ('' != $options['bscripts'] ) { echo do_shortcode($options['bscripts']);
} ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/all.js#xfbml=1";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="wrapper" class="hfeed">
<?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); ?>
<header id="header" style="background-image:url(<?php if (has_post_thumbnail() ) { echo esc_url($src[0]);
} elseif ($lawyerwp_header_url = get_post_meta($post->ID, 'lawyerwp_header_url', true) ) { echo esc_url($lawyerwp_header_url);
} elseif (get_header_image() ) { echo esc_url(get_header_image());
} else { echo esc_url(get_template_directory_uri()) . '/images/bg.jpg';
} ?>)">
<div id="header-nav">
<?php
if ($options['social'] ) {
    echo '<div id="social">';
    if ($options['facebookurl']!="" ) {
        echo '<a href="' . esc_url($options['facebookurl']) . '" id="social-facebook" rel="me">' . file_get_contents(get_template_directory_uri(). '/images/facebook.svg') . '</a>';
    }
    if ($options['twitterurl']!="" ) {
        echo '<a href="' . esc_url($options['twitterurl']) . '" id="social-twitter" rel="me">' . file_get_contents(get_template_directory_uri(). '/images/twitter.svg') . '</a>';
    }
    if ($options['googleurl']!="" ) {
        echo '<a href="' . esc_url($options['googleurl']) . '" id="social-google" rel="me">' . file_get_contents(get_template_directory_uri(). '/images/google.svg') . '</a>';
    }
    if ($options['linkedinurl']!="" ) {
        echo '<a href="' . esc_url($options['linkedinurl']) . '" id="social-linkedin" rel="me">' . file_get_contents(get_template_directory_uri(). '/images/linkedin.svg') . '</a>';
    }
    echo '' . do_shortcode(stripslashes(wp_kses_post($options['custicons']))) . '';
    echo '</div>';
}
?>
    <div id="site-title">
<?php
    $custom_logo_id = get_theme_mod('custom_logo');
$logo = wp_get_attachment_image_src($custom_logo_id, 'full');
echo '<a href="' . esc_url(home_url('/')) . '" title="' . esc_attr(get_bloginfo('name')) . '" rel="home">';
if (has_custom_logo() ) {
    echo '<img src="' . esc_url($logo[0]) . '" id="logo" />';
} else {
    bloginfo('name');
}
echo '</a>';
?>
</div>
    <nav id="menu">
	<!-- Turn off search bar for static pages
    <div id="search">
	/* <?php get_search_form(); ?> */
    </div>
	-->
    <label class="toggle" for="toggle"><span class="menu-icon">&#9776;</span> <?php esc_html_e('Menu', 'lawyerpro'); ?></label>
    <input id="toggle" class="toggle" type="checkbox" />
    <?php wp_nav_menu(array( 'theme_location' => 'main-menu' )); ?>
    </nav>
    </div>
    <?php if (is_front_page() ) { ?>
        <?php if ($options['consult'] ) { echo do_shortcode('<div id="consult-form">[contact-form-7 title="Header Consult Form"]</div>');
        } ?>
    <?php } ?>
	<div id="site-description">
    <?php if (is_front_page() ) { ?>
		<?php if ($lawyerwp_header_text = get_post_meta($post->ID, 'lawyerwp_header_text', true) ) { echo esc_html($lawyerwp_header_text);
		} else { bloginfo('description');
		} ?>
		<div class="header__cta">
			<a class="header__button button" href="<?php echo esc_url( get_permalink( 173 ) ); ?>">
				Skontaktuj się z nami
			</a>
		</div>
    <?php } ?>
	</div>
    <?php if (is_active_sidebar('header-widget-area') ) : ?>
    <aside id="header-sidebar" role="complementary">
    <div id="hsidebar" class="widget-area">
    <ul class="xoxo">
        <?php dynamic_sidebar('header-widget-area'); ?>
    </ul>
    <div class="clear"></div>
    </div>
    </aside>
    <?php endif; ?>
    </header>
    <?php if (is_front_page() ) { ?>
        <?php if ($options['hero'] ) {
            echo '<div id="hero" style="background-color:#' . esc_html(sanitize_text_field($options['herobgc'])) . ';background-image:url(' . esc_url($options['herobg']) . ')">';
            echo '<h1>' . esc_html(sanitize_text_field($options['heroh1'])) . '</h1>';
            echo '<h2>' . esc_html(sanitize_text_field($options['heroh2'])) . '</h2>';
            echo '<p>' . esc_html(sanitize_text_field($options['herop'])) . '</p>';
            echo '<a href="' . esc_url($options['herourl']) . '" class="cta button">' . esc_html(sanitize_text_field($options['herocta'])) . '</a>';
            echo '</div>';
        } ?>
        <?php if ($options['slider'] ) {
            echo '<div id="slider">';
            if (shortcode_exists('meteor_slideshow') ) {
                echo do_shortcode('[meteor_slideshow]');
            }
            echo '</div>';
        } ?>
        <?php if ($options['twitter'] ) {
            echo '<div id="twitter-feed">';
            echo '<a class="twitter-timeline" data-theme="light" data-link-color="#00b4ff" data-chrome="noheader nofooter noborders noscrollbar transparent" data-tweet-limit="' . esc_html(sanitize_text_field($options['tweetcount'])) . '" data-show-replies="false" data-screen-name="' . esc_html(sanitize_text_field($options["twitname"])) . '" href="https://twitter.com/' . esc_html(sanitize_text_field($options["twitname"])) . '">Tweets by @' . esc_html(sanitize_text_field($options["twitname"])) . '</a>';
            echo '</div>';
        } ?>
    <?php } ?>
<?php if ($options['crumbs'] ) { lawyerwp_breadcrumbs();
} ?>
<div id="container">
