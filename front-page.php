<?php get_header(); ?>

<section class="container offer">
	<h2 class="offer__title">Specjalizacje</h2>
	<div class="offer__row boxes">
		<div class="offer__box box-4">
			<span class="offer__icon fas fa-building"></span>
			<h5 class="offer__area">Nieruchomości</h5>
			<p>Sed sit amet arcu ipsum. Aliquam ornare dapibus vestibulum.</p>
		</div>
		<div class="offer__box box-4">
			<span class="offer__icon fas fa-money-bill"></span>
			<h5 class="offer__area">Prawo spadkowe</h5>
			<p>Sed sit amet arcu ipsum. Aliquam ornare dapibus vestibulum.</p>
		</div>
		<div class="offer__box box-4">
			<span class="offer__icon fas fa-user"></span>
			<h5 class="offer__area">Prawo rodzinne</h5>
			<p>Sed sit amet arcu ipsum. Aliquam ornare dapibus vestibulum.</p>
		</div>
		<div class="offer__box box-4">
			<span class="offer__icon fas fa-file-alt"></span>
			<h5 class="offer__area">Prawo handlowe</h5>
			<p>Sed sit amet arcu ipsum. Aliquam ornare dapibus vestibulum.</p>
		</div>
	</div>
	<div class="offer__row boxes">
		<div class="offer__more box">
			<a class="offer__button button" href="<?php echo esc_url( get_permalink( 131 ) ); ?>">
				Więcej
			</a>
		</div>
	</div>
</section>

<section class="container about">
	<div class="about__row boxes">
		<div class="about__frame float-right box-2">
			<img class="about__image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/office.jpeg"
			alt="korytarz w biurze">
		</div>
		<div class="about__box box-2">
			<h2 class="about__title">O nas</h2>
			<p class="about__paragraph">Kancelaria istnieje od 2018 roku, congue eu lacinia porttitor, vulputate aliquet arcu. Suspendisse convallis, lorem nec venenatis porttitor, erat dolor dapibus felis, vitae malesuada felis elit at augue. Cras quis fringilla purus. Pellentesque et ipsum quis lectus rutrum mattis ut vitae augue. Pellentesque in libero porta, scelerisque eros vel, placerat massa. Nullam accumsan odio non enim viverra condimentum in in lorem. In arcu turpis, egestas quis elit faucibus, gravida imperdiet enim. Mauris at lectus nec enim rhoncus ultrices et et lacus.</p>
			<a class="about__button button" href="<?php echo esc_url( get_permalink( 2 ) ); ?>">
				Więcej
			</a>
		</div>
	</div>
</section>

<section class="container team">
	<div class="team__row boxes">
		<div class="team__frame float-left box-2">
			<img class="team__image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/team.jpeg"
			alt="ludzie siedzący przed komputerem">
		</div>
		<div class="team__box box-2">
			<h2 class="team__title">Zespół</h2>
			<p class="team__paragraph">Duis volutpat non enim volutpat auctor. Vivamus ornare vestibulum elit ut tincidunt. Ut et egestas erat. Cras ut posuere dui. In at urna sit amet sapien vestibulum condimentum at eu ipsum. Phasellus faucibus congue leo ac blandit. Praesent libero augue, volutpat sed ipsum vel, tempor rhoncus orci. Nunc aliquam sollicitudin mollis. Proin tincidunt lacinia gravida. Sed pulvinar augue at odio sollicitudin, a lacinia quam tristique. Morbi iaculis elementum gravida.</p>
			<a class="team__button button" href="<?php echo esc_url( get_permalink( 221 ) ); ?>">
				Więcej
			</a>
		</div>
	</div>
</section>

<section class="container contact">
	<div class="contact__row boxes">
		<div class="contact__box float-right box-2">
			<iframe class="contact__map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=20.97812533378601%2C52.23022429716012%2C20.990098714828495%2C52.23410119865298&amp;layer=mapnik&amp;marker=52.23216279022971%2C20.98411202430725">
			</iframe>
			<br/>
			<small class="contact__link">
				<a href="https://www.openstreetmap.org/?mlat=52.23216&amp;mlon=20.98411#map=17/52.23216/20.98411" target="_blank">
					większa mapa
				</a>
			</small>
		</div>
		<div class="contact__box box-2">
			<h2 class="contact__title">Skontaktuj się z nami</h2>
			<p class="contact__paragraph">Duis volutpat non enim volutpat auctor. Vivamus ornare vestibulum elit ut tincidunt. Ut et egestas erat. Cras ut posuere dui. In at urna sit amet sapien vestibulum condimentum at eu ipsum. Phasellus faucibus congue leo ac blandit. Praesent libero augue, volutpat sed ipsum vel, tempor rhoncus orci. Nunc aliquam sollicitudin mollis. Proin tincidunt lacinia gravida. Sed pulvinar augue at odio sollicitudin, a lacinia quam tristique. Morbi iaculis elementum gravida.</p>
			<a class="contact__button button" href="<?php echo esc_url( get_permalink( 173 ) ); ?>">
				Zadaj pytanie
			</a>
		</div>
	</div>
</section>

<?php get_footer(); ?>
