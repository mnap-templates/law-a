<?php
function lawyerwp_get_option_defaults()
{
$defaults = array(
'sidebar' => '',
'social' => '',
'socialcolor' => '',
'consult' => '',
'hero' => '',
'heroh1' => '',
'heroh2' => '',
'herop' => '',
'herocta' => '',
'herourl' => '',
'herobgc' => '',
'herobg' => '',
'slider' => '',
'twitter' => '',
'crumbs' => '',
'share' => '',
'favicon' => '',
'appleicon' => '',
'fbicon' => '',
'facebookurl' => '',
'twitterurl' => '',
'googleurl' => '',
'linkedinurl' => '',
'twitname' => '',
'tweetcount' => '',
'custicons' => '',
'copyright' => '',
'customcss' => '',
'hscripts' => '',
'bscripts' => '',
'fscripts' => '',
);
return $defaults;
}
add_action( 'admin_init', 'lawyerwp_options_init' );
function lawyerwp_options_init()
{
register_setting( 'lawyerwp_options', 'lawyerwp_options', 'lawyerwp_options_validate' );
}
add_action( 'admin_menu', 'lawyerwp_options_add_page' );
function lawyerwp_options_add_page()
{
global $lawyerwp_theme_page;
$lawyerwp_theme_page = add_menu_page( esc_html__( 'LawyerPro', 'lawyerpro' ), esc_html__( 'LawyerPro', 'lawyerpro' ), 'edit_theme_options', 'theme_options', 'lawyerwp_options_do_page' );
add_action( 'admin_print_scripts-' . $lawyerwp_theme_page, 'lawyerwp_enqueue_admin_scripts' );
}
function lawyerwp_options_do_page()
{
global $select_options;
$raw_options = get_option( 'lawyerwp_options', array() );
$options = wp_parse_args( $raw_options , lawyerwp_get_option_defaults() );
if ( ! isset( $_REQUEST['settings-updated'] ) )
$_REQUEST['settings-updated'] = false;
?>
<div id="lawyerwp-options" class="wrap">
<?php global $lawyerwp_theme_page; ?>
<?php $current_theme = wp_get_theme(); ?>
<?php echo "<h1>" . sprintf( esc_html__( '%s Settings', 'lawyerpro' ), esc_html( $current_theme->Name ) ) . "</h1>"; ?>
<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
<div class="updated fade"><p><strong><?php esc_html_e( 'Settings Saved', 'lawyerpro' ); ?></strong></p></div>
<?php endif; ?>
<p><?php printf( esc_html__( 'Thank you for choosing LawyerPro and WordPress as the solution for building your website. If you need support or custom services %1$ssign up for a subscription%2$s.', 'lawyerpro' ), '<a href="https://lawyerweb.co/" target="_blank">', '</a>' ); ?></p>
<br />
<br />
<ul class="tabs">
<li class="tab-link current" data-tab="tab-1">Features</li>
<li class="tab-link" data-tab="tab-2">Images</li>
<li class="tab-link" data-tab="tab-3">Social</li>
<li class="tab-link" data-tab="tab-4">Wording</li>
<li class="tab-link" data-tab="tab-5">Advanced</li>
<li class="tab-link" data-tab="tab-6">Import/Export</li>
</ul>
<form method="post" action="options.php">
<p class="submit">
<input type="submit" class="button-primary" value="<?php esc_html_e( 'Save Settings', 'lawyerpro' ); ?>" />
</p>
<?php settings_fields( 'lawyerwp_options' ); ?>
<div id="tab-1" class="tab-content current">
<h2><?php esc_html_e( 'Features', 'lawyerpro' ); ?></h2>
<p><?php esc_html_e( 'Check a box to turn on that feature.', 'lawyerpro' ); ?></p>
<br />
<br />
<input id="lawyerwp_options[sidebar]" name="lawyerwp_options[sidebar]" type="checkbox" value="1" <?php checked( '1', $options['sidebar'] ); ?> />
<label class="description" for="lawyerwp_options[sidebar]"><?php esc_html_e( 'Universal Sidebar', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'show the main sidebar on all pages - add content under Appearance > Widgets > Sidebar Widget Area', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[social]" name="lawyerwp_options[social]" type="checkbox" value="1" <?php checked( '1', $options['social'] ); ?> />
<label class="description" for="lawyerwp_options[social]"><?php esc_html_e( 'Universal Social Profile Icons &amp; Custom Contact Area', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'adjust settings under the Social tab', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[crumbs]" name="lawyerwp_options[crumbs]" type="checkbox" value="1" <?php checked( '1', $options['crumbs'] ); ?> />
<label class="description" for="lawyerwp_options[crumbs]"><?php esc_html_e( 'Universal Breadcrumbs', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'shows your visitors where they are in relation to the homepage', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[consult]" name="lawyerwp_options[consult]" type="checkbox" value="1" <?php checked( '1', $options['consult'] ); ?> />
<label class="description" for="lawyerwp_options[consult]"><?php esc_html_e( 'Homepage Consultation Form', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'show a consultation form on the homepage - activate the Contact Form 7 plugin and create a form called "Header Consult Form"', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[hero]" name="lawyerwp_options[hero]" type="checkbox" value="1" <?php checked( '1', $options['hero'] ); ?> />
<label class="description" for="lawyerwp_options[hero]"><?php esc_html_e( 'Homepage Hero', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'show a big, beautiful image on your homepage with text overlaid on top - adjust settings under Wording tab', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[slider]" name="lawyerwp_options[slider]" type="checkbox" value="1" <?php checked( '1', $options['slider'] ); ?> />
<label class="description" for="lawyerwp_options[slider]"><?php esc_html_e( 'Homepage Slider', 'lawyerpro' ); ?> <em>(<?php printf(esc_html__( 'adjust settings and add new slides under Slides [from the main admin menu on the left after activating the Meteor Slides plugin from Appearance > Install Plugins]', 'lawyerpro' ) ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[twitter]" name="lawyerwp_options[twitter]" type="checkbox" value="1" <?php checked( '1', $options['twitter'] ); ?> />
<label class="description" for="lawyerwp_options[twitter]"><?php esc_html_e( 'Homepage Twitter Feed', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'adjust settings under the Social tab', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[share]" name="lawyerwp_options[share]" type="checkbox" value="1" <?php checked( '1', $options['share'] ); ?> />
<label class="description" for="lawyerwp_options[share]"><?php esc_html_e( 'Single Post Sharing', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'sets Like, Tweet, and +1 buttons on single posts', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<p><em><?php printf( esc_html__( 'Some features require additional plugins. These plugins can be found in the recommended plugins list under Appearance > Install Plugins.', 'lawyerpro' ) ); ?></em></p>
</div>
<div id="tab-2" class="tab-content">
<h2><?php esc_html_e( 'Images', 'lawyerpro' ); ?></h2>
<p><?php esc_html_e( 'Enter full image URLs or click the "Select/Upload Image" button to select/upload an image from the Media Manager and then click "Insert into Post."', 'lawyerpro' ); ?></p>
<br />
<br />
<p class="lawyerwp-options-favicon">
<label class="description" for="lawyerwp_options[favicon]"><?php esc_html_e( 'Favicon Image URL', 'lawyerpro' ); ?> <em>(<?php esc_html_e( '32x32 pixel image found in URL bar', 'lawyerpro' ); ?>)</em></label><br />
<input id="lawyerwp_options[favicon]" class="favicon-upload-url" type="text" name="lawyerwp_options[favicon]" value="<?php echo esc_attr( $options['favicon'] ); ?>" />
<input id="lawyerwp_options_favicon_button" type="button" class="button-primary" value="Upload" />
</p>
<p class="lawyerwp-options-appleicon">
<label class="description" for="lawyerwp_options[appleicon]"><?php esc_html_e( 'Apple Device Icon Image URL', 'lawyerpro' ); ?> <em>(<?php esc_html_e( '1024x1024 pixel image shown for Apple devices', 'lawyerpro' ); ?>)</em></label><br />
<input id="lawyerwp_options[appleicon]" class="appleicon-upload-url" type="text" name="lawyerwp_options[appleicon]" value="<?php echo esc_attr( $options['appleicon'] ); ?>" />
<input id="lawyerwp_options_appleicon_button" type="button" class="button-primary" value="Upload" />
</p>
<p class="lawyerwp-options-fbicon">
<label class="description" for="lawyerwp_options[fbicon]"><?php esc_html_e( 'Facebook Share Image URL', 'lawyerpro' ); ?> <em>(<?php esc_html_e( '1200x? pixel default image shown for Facebook sharing', 'lawyerpro' ); ?>)</em></label><br />
<input id="lawyerwp_options[fbicon]" class="fbicon-upload-url" type="text" name="lawyerwp_options[fbicon]" value="<?php echo esc_attr( $options['fbicon'] ); ?>" />
<input id="lawyerwp_options_fbicon_button" type="button" class="button-primary" value="Upload" />
</p>
<br />
<br />
<p><em><?php printf( esc_html__( 'Manage previously uploaded images under the %1$sMedia%2$s tab.', 'lawyerpro' ), '<a href="' . esc_url( admin_url() ) . 'upload.php" target="_blank">', '</a>' ); ?></em></p>
</div>
<div id="tab-3" class="tab-content">
<h2><?php esc_html_e( 'Social', 'lawyerpro' ); ?></h2>
<p><?php esc_html_e( 'Enter full URLs.', 'lawyerpro' ); ?></p>
<br />
<br />
<input id="lawyerwp_options[socialcolor]" class="medium-text jscolor {required:false}" type="text" name="lawyerwp_options[socialcolor]" value="<?php echo esc_attr( $options['socialcolor'] ); ?>" />
<label class="description" for="lawyerwp_options[socialcolor]"><?php esc_html_e( 'Social Icons Color', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[facebookurl]" class="regular-text" type="text" name="lawyerwp_options[facebookurl]" value="<?php echo esc_attr( $options['facebookurl'] ); ?>" />
<label class="description" for="lawyerwp_options[facebookurl]"><?php esc_html_e( 'Facebook Profile URL', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( 'example: %s', 'lawyerpro' ), '<strong>https://www.facebook.com/yourusername</strong>' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[twitterurl]" class="regular-text" type="text" name="lawyerwp_options[twitterurl]" value="<?php echo esc_attr( $options['twitterurl'] ); ?>" />
<label class="description" for="lawyerwp_options[twitterurl]"><?php esc_html_e( 'Twitter Profile URL', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( 'example: %s', 'lawyerpro' ), '<strong>https://twitter.com/yourusername</strong>' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[googleurl]" class="regular-text" type="text" name="lawyerwp_options[googleurl]" value="<?php echo esc_attr( $options['googleurl'] ); ?>" />
<label class="description" for="lawyerwp_options[googleurl]"><?php esc_html_e( 'Google+ Profile URL', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( 'example: %s', 'lawyerpro' ), '<strong>https://plus.google.com/yourusernumber</strong>' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[linkedinurl]" class="regular-text" type="text" name="lawyerwp_options[linkedinurl]" value="<?php echo esc_attr( $options['linkedinurl'] ); ?>" />
<label class="description" for="lawyerwp_options[linkedinurl]"><?php esc_html_e( 'LinkedIn Profile URL', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( 'example: %s', 'lawyerpro' ), '<strong>https://www.linkedin.com/in/yourusername</strong>' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[twitname]" class="regular-text" type="text" name="lawyerwp_options[twitname]" value="<?php echo esc_attr( $options['twitname'] ); ?>" />
<label class="description" for="lawyerwp_options[twitname]"><?php esc_html_e( 'Twitter Profile Username', 'lawyerpro' ); ?> <em>(<?php esc_html_e( 'username only - no @ needed', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<input id="lawyerwp_options[tweetcount]" class="regular-text" type="text" name="lawyerwp_options[tweetcount]" value="<?php echo esc_attr( $options['tweetcount'] ); ?>" />
<label class="description" for="lawyerwp_options[tweetcount]"><?php esc_html_e( 'Number of Tweets to Show', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( '%s is recommended for aesthetics', 'lawyerpro' ), '<strong>1</strong>' ); ?>)</em></label>
<br />
<br />
<textarea id="lawyerwp_options[custicons]" class="large-text" cols="40" rows="3" name="lawyerwp_options[custicons]"><?php echo $options['custicons']; ?></textarea>
<label class="description" for="lawyerwp_options[custicons]"><strong><?php esc_html_e( 'Custom Icons / Custom Contact', 'lawyerpro' ); ?></strong> - <?php esc_html_e( 'set your own additional icons or contact details', 'lawyerpro' ); ?><br /><em><?php printf( esc_html__( 'Icon Example: %s', 'lawyerpro' ), '<code>&lt;a href=&quot;http://profileurl.com/&quot;&gt;&lt;img src=&quot;ICON-IMAGE-URL-HERE&quot; alt=&quot;Social Network&quot; /&gt;&lt;/a&gt;</code>' ); ?><br /><?php printf( esc_html__( 'Contact Details Example: %s', 'lawyerpro' ), '<code>&lt;div class=&quot;details&quot;&gt;123.456.7890 | &lt;a href=&quot;mailto:email@website.com&quot;&gt;email@website.com&lt;/a&gt;&lt;/div&gt;</code>' ); ?></em></label>
</div>
<div id="tab-4" class="tab-content">
<h2><?php esc_html_e( 'Wording', 'lawyerpro' ); ?></h2>
<p></p>
<br />
<br />
<input id="lawyerwp_options[heroh1]" class="regular-text" type="text" name="lawyerwp_options[heroh1]" value="<?php echo esc_attr( $options['heroh1'] ); ?>" />
<label class="description" for="lawyerwp_options[heroh1]"><?php esc_html_e( 'Hero Header', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[heroh2]" class="regular-text" type="text" name="lawyerwp_options[heroh2]" value="<?php echo esc_attr( $options['heroh2'] ); ?>" />
<label class="description" for="lawyerwp_options[heroh2]"><?php esc_html_e( 'Hero Sub-header', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[herop]" class="regular-text" type="text" name="lawyerwp_options[herop]" value="<?php echo esc_attr( $options['herop'] ); ?>" />
<label class="description" for="lawyerwp_options[herop]"><?php esc_html_e( 'Hero Paragraph', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[herocta]" class="regular-text" type="text" name="lawyerwp_options[herocta]" value="<?php echo esc_attr( $options['herocta'] ); ?>" />
<label class="description" for="lawyerwp_options[herocta]"><?php esc_html_e( 'Hero Call-to-Action', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[herourl]" class="regular-text" type="text" name="lawyerwp_options[herourl]" value="<?php echo esc_attr( $options['herourl'] ); ?>" />
<label class="description" for="lawyerwp_options[herourl]"><?php esc_html_e( 'Hero Call-to-Action Link', 'lawyerpro' ); ?></label>
<br />
<br />
<input id="lawyerwp_options[herobgc]" class="regular-text jscolor {required:false}" type="text" name="lawyerwp_options[herobgc]" value="<?php echo esc_attr( $options['herobgc'] ); ?>" />
<label class="description" for="lawyerwp_options[herobgc]"><?php esc_html_e( 'Hero Background Color', 'lawyerpro' ); ?></label>
<br />
<br />
<p class="lawyerwp-options-herobg">
<input id="lawyerwp_options[herobg]" class="herobg-upload-url" type="text" name="lawyerwp_options[herobg]" value="<?php echo esc_attr( $options['herobg'] ); ?>" />
<input id="lawyerwp_options_herobg_button" type="button" class="button-primary" value="Upload" />
<label class="description" for="lawyerwp_options[herobg]"><?php esc_html_e( 'Hero Background Image', 'lawyerpro' ); ?></label>
</p>
<br />
<br />
<textarea id="lawyerwp_options[copyright]" class="large-text" cols="40" rows="2" name="lawyerwp_options[copyright]"><?php echo $options['copyright']; ?></textarea>
<label class="description" for="lawyerwp_options[copyright]"><strong>&copy; <?php esc_html_e( 'Copyright Wording', 'lawyerpro' ); ?></strong> - <?php esc_html_e( 'Or use this space for something else.', 'lawyerpro' ); ?></label>
</div>
<div id="tab-5" class="tab-content">
<h2><?php esc_html_e( 'Advanced', 'lawyerpro' ); ?></h2>
<p></p>
<br />
<br />
<textarea id="lawyerwp_options[customcss]" class="large-text" cols="40" rows="8" name="lawyerwp_options[customcss]"><?php echo $options['customcss']; ?></textarea>
<label class="description" for="lawyerwp_options[customcss]"><strong><?php esc_html_e( 'Custom CSS', 'lawyerpro' ); ?></strong> - <?php esc_html_e( 'For placement of custom CSS.', 'lawyerpro' ); ?> <em>(<?php printf( esc_html__( 'for reference, %1$sview%2$s the default stylesheet', 'lawyerpro' ), '<a href="' . esc_url( get_stylesheet_uri() ) . '" target="_blank">', '</a>' ); ?>)</em></label>
<br />
<br />
<textarea id="lawyerwp_options[hscripts]" class="large-text" cols="40" rows="8" name="lawyerwp_options[hscripts]"><?php echo $options['hscripts']; ?></textarea>
<label class="description" for="lawyerwp_options[hscripts]"><strong><?php esc_html_e( 'Header Scripts', 'lawyerpro' ); ?></strong> - <?php printf( esc_html__( 'For placement of scripts and code just before the %s tag.', 'lawyerpro' ), '<code>&lt;/head&gt;</code>' ); ?> <em>(<?php esc_html_e( 'Google Analytics script goes here', 'lawyerpro' ); ?>)</em></label>
<br />
<br />
<textarea id="lawyerwp_options[bscripts]" class="large-text" cols="40" rows="8" name="lawyerwp_options[bscripts]"><?php echo $options['bscripts']; ?></textarea>
<label class="description" for="lawyerwp_options[bscripts]"><strong><?php esc_html_e( 'Body Scripts', 'lawyerpro' ); ?></strong> - <?php printf( esc_html__( 'For placement of scripts and code just after the %s tag.', 'lawyerpro' ), '<code>&lt;body&gt;</code>' ); ?></label>
<br />
<br />
<textarea id="lawyerwp_options[fscripts]" class="large-text" cols="40" rows="8" name="lawyerwp_options[fscripts]"><?php echo $options['fscripts']; ?></textarea>
<label class="description" for="lawyerwp_options[fscripts]"><strong><?php esc_html_e( 'Footer Scripts', 'lawyerpro' ); ?></strong> - <?php printf( esc_html__( 'For placement of scripts and code just before the %s tag.', 'lawyerpro' ), '<code>&lt;/body&gt;</code>' ); ?></label>
</div>
<div id="tab-6" class="tab-content">
<h2><?php esc_html_e( 'Import & Export Theme Settings', 'lawyerpro' ); ?></h2>
<p><?php esc_html_e( 'To import, simply paste in the settings from another install or backup below and click "Import." To export, simply copy out these settings to a new text file or theme install.', 'lawyerpro' ); ?></p>
<br />
<br />
<textarea id="lawyerwp_options_import_export_settings" class="large-text" cols="40" rows="8" onclick="this.select()"><?php echo serialize( $raw_options ); ?></textarea>
<p class="submit">
<input id="lawyerwp_options_import_button" type="button" class="button-primary" value="Import">
</p>
</div>
<p class="submit">
<input type="submit" class="button-primary" value="<?php esc_html_e( 'Save Settings', 'lawyerpro' ); ?>" />
</p>
</form>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script>
jQuery(document).ready(function(){
jQuery('ul.tabs li').click(function(){
var tab_id = $(this).attr('data-tab');
jQuery('ul.tabs li').removeClass('current');
jQuery('.tab-content').removeClass('current');
jQuery(this).addClass('current');
jQuery("#"+tab_id).addClass('current');
})
})
</script>
</div>
<?php
}
function lawyerwp_options_validate( $input )
{
$valid_input = wp_parse_args( get_option( 'lawyerwp_options', array() ), lawyerwp_get_option_defaults() );
$valid_input['sidebar'] = ( isset( $input['sidebar'] ) && true == $input['sidebar'] ? true : false );
$valid_input['social'] = ( isset( $input['social'] ) && true == $input['social'] ? true : false );
$valid_input['consult'] = ( isset( $input['consult'] ) && true == $input['consult'] ? true : false );
$valid_input['hero'] = ( isset( $input['hero'] ) && true == $input['hero'] ? true : false );
$valid_input['slider'] = ( isset( $input['slider'] ) && true == $input['slider'] ? true : false );
$valid_input['twitter'] = ( isset( $input['twitter'] ) && true == $input['twitter'] ? true : false );
$valid_input['crumbs'] = ( isset( $input['crumbs'] ) && true == $input['crumbs'] ? true : false );
$valid_input['share'] = ( isset( $input['share'] ) && true == $input['share'] ? true : false );
$valid_input['favicon'] = ( isset( $input['favicon'] ) ? esc_url_raw( $input['favicon'] ) : $valid_input['favicon'] );
$valid_input['appleicon'] = ( isset( $input['appleicon'] ) ? esc_url_raw( $input['appleicon'] ) : $valid_input['appleicon'] );
$valid_input['fbicon'] = ( isset( $input['fbicon'] ) ? esc_url_raw( $input['fbicon'] ) : $valid_input['fbicon'] );
$valid_input['herobg'] = ( isset( $input['herobg'] ) ? esc_url_raw( $input['herobg'] ) : $valid_input['herobg'] );
$valid_input['facebookurl'] = ( isset( $input['facebookurl'] ) ? esc_url_raw( $input['facebookurl'] ) : $valid_input['facebookurl'] );
$valid_input['twitterurl'] = ( isset( $input['twitterurl'] ) ? esc_url_raw( $input['twitterurl'] ) : $valid_input['twitterurl'] );
$valid_input['linkedinurl'] = ( isset( $input['linkedinurl'] ) ? esc_url_raw( $input['linkedinurl'] ) : $valid_input['linkedinurl'] );
$valid_input['googleurl'] = ( isset( $input['googleurl'] ) ? esc_url_raw( $input['googleurl'] ) : $valid_input['googleurl'] );
$valid_input['herourl'] = ( isset( $input['herourl'] ) ? esc_url_raw( $input['herourl'] ) : $valid_input['herourl'] );
$valid_input['twitname'] = ( isset( $input['twitname'] ) ? wp_filter_nohtml_kses( $input['twitname'] ) : $valid_input['twitname'] );
$valid_input['heroh1'] = ( isset( $input['heroh1'] ) ? wp_filter_nohtml_kses( $input['heroh1'] ) : $valid_input['heroh1'] );
$valid_input['heroh2'] = ( isset( $input['heroh2'] ) ? wp_filter_nohtml_kses( $input['heroh2'] ) : $valid_input['heroh2'] );
$valid_input['herop'] = ( isset( $input['herop'] ) ? wp_filter_nohtml_kses( $input['herop'] ) : $valid_input['herop'] );
$valid_input['herocta'] = ( isset( $input['herocta'] ) ? wp_filter_nohtml_kses( $input['herocta'] ) : $valid_input['herocta'] );
$valid_input['tweetcount'] = ( isset( $input['tweetcount'] ) && is_int( intval( $input['tweetcount'] ) ) ? $input['tweetcount'] : $valid_input['tweetcount'] );
if ( ! isset( $input['socialcolor'] ) || '' == $input['socialcolor'] ) {
$valid_input['socialcolor'] = '';
} else {
$input['socialcolor'] = ltrim( trim( $input['socialcolor' ] ), '#' );
$input['socialcolor'] = ( 6 == strlen( $input['socialcolor'] ) ? $input['socialcolor'] : $valid_input['socialcolor'] );
$valid_input['socialcolor'] = ( ctype_xdigit( $input['socialcolor'] ) ? $input['socialcolor'] : $valid_input['socialcolor'] );
}
if ( ! isset( $input['herobgc'] ) || '' == $input['herobgc'] ) {
$valid_input['herobgc'] = '';
} else {
$input['herobgc'] = ltrim( trim( $input['herobgc' ] ), '#' );
$input['herobgc'] = ( 6 == strlen( $input['herobgc'] ) ? $input['herobgc'] : $valid_input['herobgc'] );
$valid_input['herobgc'] = ( ctype_xdigit( $input['herobgc'] ) ? $input['herobgc'] : $valid_input['herobgc'] );
}
$valid_input['custicons'] = ( isset( $input['custicons'] ) ? $input['custicons'] : $valid_input['custicons'] );
$valid_input['copyright'] = ( isset( $input['copyright'] ) ? $input['copyright'] : $valid_input['copyright'] );
$valid_input['customcss'] = ( isset( $input['customcss'] ) ? $input['customcss'] : $valid_input['customcss'] );
$valid_input['hscripts'] = ( isset( $input['hscripts'] ) ? $input['hscripts'] : $valid_input['hscripts'] );
$valid_input['bscripts'] = ( isset( $input['bscripts'] ) ? $input['bscripts'] : $valid_input['bscripts'] );
$valid_input['fscripts'] = ( isset( $input['fscripts'] ) ? $input['fscripts'] : $valid_input['fscripts'] );
return $valid_input;
}