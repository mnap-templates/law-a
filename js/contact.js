jQuery(document).ready(function($){
$('#contactform').submit(function(){
var action = $(this).attr('action');
$("#message").slideUp(200,function(){
$('#message').hide();
$('#submit')
.after('')
.attr('disabled','disabled');
$.post(action,{
name: $('#name').val(),
email: $('#email').val(),
phone: $('#phone').val(),
subject: $('#subject').val(),
comments: $('#comments').val(),
verify: $('#verify').val()
},
function(data){
document.getElementById('message').innerHTML = data;
$('#message').slideDown('fast');
$('#contactform img.loader').fadeOut('fast',function(){$(this).remove()});
$('#submit').removeAttr('disabled');
if(data.match('success') != null) $('#contactform').slideUp('fast');
}
);
});
return false;
});
});