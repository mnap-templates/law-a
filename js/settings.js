jQuery(document).ready(function() {
function import_settings() {
button = '#lawyerwp_options_import_button';
if(jQuery(button)) {
jQuery(button).click(function() {
var r = confirm(lawyer.import_confirm);
if (r == true) {
var import_settings = jQuery('#lawyerwp_options_import_export_settings').val();
jQuery.post(ajaxurl, { action:'lawyerwp_import_settings', lawyerwp_settings: import_settings }, 
function(data){ if (data.redirect) { window.location = data.redirect; 
}
} , 'json'); 
return false;
}
return false;
});
}
}
function set_uploader_favicon() {
button = '#lawyerwp_options_favicon_button';
if(jQuery(button)) {
jQuery(button).click(function() {
tb_show('', 'media-upload.php?post_id=0&type=image&amp;TB_iframe=true');
set_send_favicon();
return false;
});
}
}
function set_send_favicon() {
window.original_send_to_editor = window.send_to_editor;
window.send_to_editor = function(html) {
if ( jQuery(html).is("a") ) {
var imgurl = jQuery('img', html).attr('src');
} else if ( jQuery(html).is("img") ) {
var imgurl = jQuery(html).attr('src');
}
jQuery('.favicon-upload-url').val(imgurl);
tb_remove();
window.send_to_editor = window.original_send_to_editor;
};
}
function set_uploader_appleicon() {
button = '#lawyerwp_options_appleicon_button';
if(jQuery(button)) {
jQuery(button).click(function() {
tb_show('', 'media-upload.php?post_id=0&type=image&amp;TB_iframe=true');
set_send_appleicon();
return false;
});
}
}
function set_send_appleicon() {
window.original_send_to_editor = window.send_to_editor;
window.send_to_editor = function(html) {
if ( jQuery(html).is("a") ) {
var imgurl = jQuery('img', html).attr('src');
} else if ( jQuery(html).is("img") ) {
var imgurl = jQuery(html).attr('src');
}
jQuery('.appleicon-upload-url').val(imgurl);
tb_remove();
window.send_to_editor = window.original_send_to_editor;
};
}
function set_uploader_fbicon() {
button = '#lawyerwp_options_fbicon_button';
if(jQuery(button)) {
jQuery(button).click(function() {
tb_show('', 'media-upload.php?post_id=0&type=image&amp;TB_iframe=true');
set_send_fbicon();
return false;
});
}
}
function set_send_fbicon() {
window.original_send_to_editor = window.send_to_editor;
window.send_to_editor = function(html) {
if ( jQuery(html).is("a") ) {
var imgurl = jQuery('img', html).attr('src');
} else if ( jQuery(html).is("img") ) {
var imgurl = jQuery(html).attr('src');
}
jQuery('.fbicon-upload-url').val(imgurl);
tb_remove();
window.send_to_editor = window.original_send_to_editor;
};
}
function set_uploader_herobg() {
button = '#lawyerwp_options_herobg_button';
if(jQuery(button)) {
jQuery(button).click(function() {
tb_show('', 'media-upload.php?post_id=0&type=image&amp;TB_iframe=true');
set_send_herobg();
return false;
});
}
}
function set_send_herobg() {
window.original_send_to_editor = window.send_to_editor;
window.send_to_editor = function(html) {
if ( jQuery(html).is("a") ) {
var imgurl = jQuery('img', html).attr('src');
} else if ( jQuery(html).is("img") ) {
var imgurl = jQuery(html).attr('src');
}
jQuery('.herobg-upload-url').val(imgurl);
tb_remove();
window.send_to_editor = window.original_send_to_editor;
};
}
set_uploader_favicon();
set_uploader_appleicon();
set_uploader_fbicon();
set_uploader_herobg();
import_settings();
});