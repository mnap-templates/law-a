<?php
if ( ! class_exists( 'SellwireThemeUpdater_4w6q' ) ) {
class SellwireThemeUpdater_4w6q {
var $api_url;
var $theme_id = '4w6q';
var $theme_slug;
var $license_key;
function __construct( $api_url, $theme_slug, $license_key = null ) {
$this->api_url     = $api_url;
$this->theme_slug  = $theme_slug;
$this->license_key = $license_key;
add_filter( 'pre_set_site_transient_update_themes', array( &$this, 'check_for_update' ) );
}
function check_for_update( $transient ) {
if ( empty( $transient->checked ) ) {
return $transient;
}
$request_args = array(
'id'      => $this->theme_id,
'slug'    => $this->theme_slug,
'version' => $transient->checked[ $this->theme_slug ],
);
if ( $this->license_key ) {
$request_args['license'] = $this->license_key;
}
$request_string = $this->prepare_request( 'theme_update', $request_args );
$raw_response   = wp_remote_post( $this->api_url, $request_string );
$response = null;
if ( ! is_wp_error( $raw_response ) && ( $raw_response['response']['code'] == 200 ) ) {
$response = unserialize( $raw_response['body'] );
}
if ( ! empty( $response ) )
{
$transient->response[ $this->theme_slug ] = $response;
}
return $transient;
}
function prepare_request( $action, $args ) {
global $wp_version;
return array(
'body'       => array(
'action'  => $action,
'request' => serialize( $args ),
'api-key' => md5( home_url() ),
),
'user-agent' => 'WordPress/' . $wp_version . '; ' . home_url(),
);
}
}
}