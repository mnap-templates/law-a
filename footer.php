<?php $options = get_option( 'lawyerwp_options' ); ?>
</div>
<footer id="footer">
<?php if ( is_active_sidebar( 'footer-widget-area' ) ) : ?>
<aside id="footer-sidebar" role="complementary">
<div id="fsidebar" class="widget-area">
<ul class="xoxo">
<?php dynamic_sidebar( 'footer-widget-area' ); ?>
</ul>
<div class="clear"></div>
</div>
</aside>
<?php endif; ?>
<div class="footer__row boxes">
	<div class="footer__details footer__column box-3 float-left">
		<p class="footer__paragraph">Kancelaria</p>
		<p class="footer__paragraph">Plac Europejski 1</p>
		<p class="footer__paragraph">00-844 Warszawa</p>
		<p class="footer__paragraph">
			<span>Tel.:</span>
			<a class="footer__link" href="tel:+48211112233">+48 21 111 22 33</a>
		</p>
		<p class="footer__paragraph">
			<span>Kom.:</span>
			<a class="footer__link" href="tel:+48459444555">+48 459 444 555</a>
		</p>
		<p class="footer__paragraph">
			<span>E-mail:</span>
			<a class="footer__link" href="mailto:kancelaria@example.com">kancelaria@example.com</a>
		</p>
	</div>
	<div class="footer__nav footer__column box-3">
		<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
		<div id="fmenu">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'footer-menu',
					'depth'          => '1',
				) );
			?>
		</div>
		<?php endif; ?>
	</div>
	<div class="footer__social footer__column box-3 float-right">
		<?php
			echo '<a class="footer__icon" href="' . esc_url( $options['facebookurl'] ) . '" id="social-facebook" rel="me">' . file_get_contents( get_template_directory_uri(). '/images/facebook.svg' ) . '</a>';
		?>
		<?php
			echo '<a class="footer__icon" href="' . esc_url( $options['linkedinurl'] ) . '" id="social-linkedin" rel="me">' . file_get_contents( get_template_directory_uri(). '/images/linkedin.svg' ) . '</a>';
		?>
	</div>
</div>
<div id="copyright">
<?php if ( $options['copyright']!="" ) echo do_shortcode( $options['copyright'] ); else echo sprintf( esc_html__( '%1$s %2$s %3$s', 'lawyerpro' ), '&copy;', esc_html( date_i18n( __( 'Y', 'lawyerpro' ) ) ), esc_html( get_bloginfo( 'name' ) ) ); ?>
<span> | AxIT | mnap </span>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
